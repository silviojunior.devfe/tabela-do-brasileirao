import React from 'react';
import './List.css';

export default ({ data }) => {
    const context = require.context('../../assets/img', true, /.png$/);
    const images = [];
    context.keys().forEach((key) => {
        const fileName = key.replace('./', '');
        const resource = require(`../../assets/img/${fileName}`);
        images.push({ name: fileName, resource });
    });

    const removeSpecialCharacters = (string) => {
        string = string.replace(/[ÀÁÂÃÄÅ]/gi, "a");
        string = string.replace(/[àáâã]/gi, "a");
        string = string.replace(/[ÈÉÊ]/gi, "e");
        string = string.replace(/[èéê]/gi, "e");
        string = string.replace(/[ÌÍÎ]/gi, "i");
        string = string.replace(/[ìíî]/gi, "i");
        string = string.replace(/[ÒÓÔÕ]/gi, "o");
        string = string.replace(/[òóôõ]/gi, "o");
        string = string.replace(/[ÙÚÛ]/gi, "u");
        string = string.replace(/[ùúû]/gi, "u");
        string = string.replace(/[ç]/gi, "c");
        string = string.replace(/[Ç]/gi, "c");
        return string;
    };

    const getTeamImage = (teamName) => {
        return images.filter((image) => image.name.includes(removeSpecialCharacters(teamName.toLowerCase()).replace(" ", "_")))[0].resource.default;
    };

    return (
        <div>
            <table>
                <thead>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th>Pontos Ganhos</th>
                        <th>Vitórias</th>
                        <th>Empates</th>
                        <th>Derrotas</th>
                        <th>Gols Pró</th>
                        <th>Gols Contra</th>
                        <th>Saldo de Gols</th>
                    </tr>
                </thead>
                <tbody>
                    {data.map((item, index) => (
                        <tr key={item.time}>
                            <td>{index + 1}</td>
                            <td><img src={getTeamImage(item.time)} /></td>
                            <td>{item.time}</td>
                            <td>{item.totalPoints}</td>
                            <td>{item.totalVictories}</td>
                            <td>{item.totalDraws}</td>
                            <td>{item.totalDefeats}</td>
                            <td>{item.totalGoalsScored}</td>
                            <td>{item.totalGoalsConceded}</td>
                            <td>{item.totalGoalsScored - item.totalGoalsConceded}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
};