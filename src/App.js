import React, { useEffect, useState } from 'react';
import axios from 'axios';
import List from './components/List/List';

export default () => {
  const [data, setData] = useState([]);
  const [selectedYear, setSelectedYear] = useState("2003");

  useEffect(() => {
    getYearData(selectedYear);
  }, [selectedYear]);

  const handleSelectedDateChange = (e) => {
    setSelectedYear(e.target.value)
  }

  const getYearData = (year) => {
    axios.get(`http://localhost:3000/${year}`)
      .then((response) => {
        setData(orderByPoints(getFinalData(response.data)));
      })
      .catch((error) => {
        console.log(error);
      })
  }

  const getArrayOfInterval = (firstValue, lastValue) => {
    let array = [];
    for (let i = firstValue; i <= lastValue; i++) {
      array.push(i);
    }
    return array;
  }

  const getFinalData = (array) => {
    let allTeams = [];
    let homeTeam = {};
    let awayTeam = {};

    array[(array.length - 1)].partidas.map((partida) => {
      awayTeam = {
        time: partida.visitante,
        totalGoalsScored: partida.pontuacao_geral_visitante.total_gols_marcados,
        totalGoalsConceded: partida.pontuacao_geral_visitante.total_gols_sofridos,
        totalMatches: partida.pontuacao_geral_visitante.total_jogos,
        totalPoints: partida.pontuacao_geral_visitante.total_pontos,
        totalVictories: partida.pontuacao_geral_visitante.total_vitorias,
        totalDefeats: partida.pontuacao_geral_visitante.total_derrotas,
        totalDraws: partida.pontuacao_geral_visitante.total_empates,
      }
      homeTeam = {
        time: partida.mandante,
        totalGoalsScored: partida.pontuacao_geral_mandante.total_gols_marcados,
        totalGoalsConceded: partida.pontuacao_geral_mandante.total_gols_sofridos,
        totalMatches: partida.pontuacao_geral_mandante.total_jogos,
        totalPoints: partida.pontuacao_geral_mandante.total_pontos,
        totalVictories: partida.pontuacao_geral_mandante.total_vitorias,
        totalDefeats: partida.pontuacao_geral_mandante.total_derrotas,
        totalDraws: partida.pontuacao_geral_mandante.total_empates,
      }
      allTeams.push(homeTeam);
      allTeams.push(awayTeam);
    });
    return allTeams;
  }

  const orderByPoints = (teams) => {
    return teams.sort((firstTeam, secondTeam) => {
      if (firstTeam.totalPoints < secondTeam.totalPoints) {
        return 1;
      }
      if (firstTeam.totalPoints > secondTeam.totalPoints) {
        return -1;
      }
      return 0;
    });
  }

  const orderAll = (teams, criteria) => {
    // Recebe uma lista de criterios para ordenar, você deve colocar na ordem inversa da que deseja
    // Exemplo: Se você deseja ordenar por Pontos, Vitórias, Empates e Derrotas, deve criar o seguinte array:
    // criteria = ["totalDefeats", "totalDraws", "totalVictories", "totalPoints"]
    let orderedTeams = teams;
    criteria.map((criterion) => {
      orderedTeams = orderedTeams.sort((firstTeam, secondTeam) => {
        if (firstTeam[criterion] < secondTeam[criterion]) {
          return 1;
        }
        if (firstTeam[criterion] > secondTeam[criterion]) {
          return -1;
        }
        return 0;
      });
    })
    return orderedTeams;
  }

  return (
    <div>
      <select value={selectedYear} onChange={(e) => handleSelectedDateChange(e)}>
        {getArrayOfInterval(2003, 2015).map((year) => <option key={year} value={year}>{year}</option>)}
      </select>
      <List data={data} />
    </div>
  );
}